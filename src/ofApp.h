#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

class ofApp : public ofBaseApp{

	public:
        ofVec3f p1;
        ofVec3f p2;
        ofVec3f u1, u2;
        ofVec3f v1, v2;
        float m1, m2;
        float t;
        float c;
    
        bool active;
    
        ofxImGui::Gui gui;
        ofRectangle mainWindowRectangle;
        ofRectangle mainRectangle;
    
		void setup();
		void update();
		void draw();
    
        void reset();
        void start();
        void go();
        void showLabels();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void drawMainWindow();
    
};
