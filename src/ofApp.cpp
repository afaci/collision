#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    gui.setup();
    
    m1 = 1.0f;
    m2 = 1.0f;
    
    c = 1.0f;
    
    t = -5.0f;
}

//--------------------------------------------------------------
void ofApp::update(){
    float dt = ofClamp(ofGetLastFrameTime(), 0.0, 0.02);
    if(dt > 0 && active) {
        t += dt;
        if(t >= 5.0f) {
            t = 5.0f;
            active = false;
        }
    }
    
    v1 = ((m1 - c * m2)/(m1 + m2)) * u1 + ((m2 * (1 + c)) / (m1 + m2)) * u2;
    v2 = ((m1 * (1 + c)) / (m1 + m2)) * u1 + ((m2 - c * m1) / (m1 + m2)) * u2;
    
    if(t < 0) {
        p1 = t * u1;
        p2 = t * u2;
    } else {
        p1 = t * v1;
        p2 = t * v2;
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    float width = ofGetWidth() - mainWindowRectangle.width;
    float height = ofGetHeight();
    float sideLength = min(width, height);
    
    mainRectangle.setPosition(mainWindowRectangle.width, 0);
    mainRectangle.setSize(sideLength, sideLength);
    ofSetColor(255, 255, 255);
    ofFill();
    ofDrawRectangle(mainRectangle);
    ofNoFill();
    ofSetColor(0, 0, 255);
    ofDrawRectangle(mainRectangle);
    
    ofVec3f middlePoint;
    middlePoint.x = mainRectangle.width / 2 + mainRectangle.x;
    middlePoint.y = mainRectangle.height / 2 + mainRectangle.y;
    
    ofFill();
    ofTranslate(middlePoint.x, middlePoint.y);
    ofScale(mainRectangle.width / (5 + 5), mainRectangle.height / (5 + 5) * (-1));
    ofSetCircleResolution(40);
    ofNoFill();
    ofSetColor(0, 0, 0);
    ofDrawCircle(0, 0, 5.0f);
    ofSetColor(128, 128, 128);
    ofDrawCircle(0, 0, 4.0f);
    ofSetColor(0, 0, 0);
    ofDrawCircle(0, 0, 3.0f);
    ofSetColor(128, 128, 128);
    ofDrawCircle(0, 0, 2.0f);
    ofSetColor(0, 0, 0);
    ofDrawCircle(0, 0, 1.0f);
    ofFill();
    
    ofSetColor(255,0, 0);
    ofDrawCircle(p1.x, p1.y, 0.05f);
    ofSetColor(0, 0, 255);
    ofDrawCircle(p2.x, p2.y, 0.05f);
    
    gui.begin();
    drawMainWindow();
    gui.end();
}

void ofApp::drawMainWindow() {
    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImVec2(300,600), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("Main")) {
        ImGui::Text("Particle 1:");
        ImGui::SliderFloat2("v1", &u1.x, -1.0f, 1.0f, "%1.3f");
        ImGui::SliderFloat("m1", &m1, 0.0f, 1.0f, "%1.3f");
        
        ImGui::Text("Particle 2:");
        
        ImGui::SliderFloat2("v2", &u2.x, -1.0f, 1.0f, "%1.3f");
        ImGui::SliderFloat("m2", &m2, -0.0f, 1.0f, "%1.3f");
        
        ImGui::SliderFloat("c", &c, 0.0f, 1.0f, "%1.3f");
        
        if(ImGui::Button("Reset")) {
            reset();
        }
        ImGui::SameLine();
        
        if(ImGui::Button(active ? "Stop" : "Start")) {
            if(active) {
                go();
            } else {
                start();
            }
        }
        ImGui::SameLine();
        if(ImGui::Button("Go")) {
            go();
        }
        ImGui::SameLine();
        if(ImGui::Button("Show Labels")) {
            showLabels();
        }
        ImGui::SliderFloat("Time", &t, -5.0f, 5.0f, "%2.3f");
        
        
        //if(ImGui::Button("Quit")) {quit();}
        /*
        if (ImGui::CollapsingHeader("Numerical Output")) {
            ImGui::Text("^d              : (%1.2f, %1.2f)", dn.x, dn.y);
            ImGui::Text("Closing Velocity: %2.2f", vc);
        }
        
        if (ImGui::CollapsingHeader("Graphical Output")) {
            ImGui::PlotLines("d", &distanceLine[0], distanceLine.size());
            ImGui::PlotLines("Closing Velocity", &velocityLine[0], velocityLine.size());
        }
        */
    }
    // store window size so that camera can ignore mouse clicks
    mainWindowRectangle.setPosition(ImGui::GetWindowPos().x,ImGui::GetWindowPos().y);
    mainWindowRectangle.setSize(ImGui::GetWindowWidth(), ImGui::GetWindowHeight());
    ImGui::End();
}

void ofApp::reset() {
    t = -5.0f;
}

void ofApp::start() {
    
}

void ofApp::go() {
    active = !active;
}

void ofApp::showLabels() {
    
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
